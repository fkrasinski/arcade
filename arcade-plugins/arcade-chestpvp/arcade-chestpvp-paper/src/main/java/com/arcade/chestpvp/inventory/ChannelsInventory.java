package com.arcade.chestpvp.inventory;

import com.arcade.base.packet.Callback;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.model.Channels;
import com.arcade.model.ServerInfo;
import com.arcade.model.packet.AckPacket;
import com.arcade.model.packet.ServerSwitchPacket;
import com.arcade.model.packet.TransferPlayerPacket;
import com.arcade.paper.base.ArcadePlugin;
import com.arcade.paper.base.helper.SerdeHelper;
import com.arcade.paper.base.model.ProxyUser;
import com.google.inject.Inject;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class ChannelsInventory {
    private static final Logger logger = LoggerFactory.getLogger(ChannelsInventory.class);

    private final JavaPlugin plugin;
    private final ArcadePlugin arcadePlugin;
    private final RedisPubSubPacket redisPubSub;

    @Inject
    public ChannelsInventory(JavaPlugin plugin, ArcadePlugin arcadePlugin, RedisPubSubPacket redisPubSub) {
        this.plugin = plugin;
        this.arcadePlugin = arcadePlugin;
        this.redisPubSub = redisPubSub;
    }

    public SimpleInventory create(List<ServerInfo> servers) {
        SimpleInventory inv = new SimpleInventory(plugin, Component.text("Channels", NamedTextColor.RED), ((int) Math.ceil((double) servers.size() / 9)) * 9);

        for (int i = 0; i < servers.size(); i++) {
            ServerInfo server = servers.get(i);
            inv.setItem(i, createChannelItem(server, i + 1), click -> {
                try {
                    var transferPacket = createTransferPacket((Player) click.getWhoClicked(), server.id(), arcadePlugin.getServerId());
                    var proxyUser = getProxyUser(click.getWhoClicked().getUniqueId());
                    redisPubSub.publishWithCallback(Channels.server(server.id()), transferPacket, (Callback<AckPacket>) ack -> {
                        redisPubSub.publish(Channels.proxy(proxyUser.getProxyId()), createServerSwitchPacket((Player) click.getWhoClicked(), server));
                    });
                } catch (IOException e) {
                    logger.error("Failed to create TransferPlayerPacket", e);
                    click.getWhoClicked().sendMessage(Component.text("Error occurred, try again later", NamedTextColor.RED));
                    click.getWhoClicked().getOpenInventory().close();
                }
            });
        }

        return inv;
    }

    private ProxyUser getProxyUser(UUID uuid) {
        return arcadePlugin.proxyUsersStorage().get(uuid);
    }

    private ItemStack createChannelItem(ServerInfo serverInfo, int index) {
        ItemStack item = new ItemStack(Material.GREEN_CONCRETE);
        ItemMeta meta = item.getItemMeta();
        meta.displayName(Component.text("Channel " + index, NamedTextColor.GREEN));
        meta.lore(List.of(
                Component.text("ID: ", NamedTextColor.GRAY).append(Component.text(serverInfo.id(), NamedTextColor.GREEN)),
                Component.text("TPS: ", NamedTextColor.GRAY).append(Component.text((int) serverInfo.tps(), NamedTextColor.GREEN)),
                Component.text("Players: ", NamedTextColor.GRAY).append(Component.text(serverInfo.playersCount(), NamedTextColor.GREEN))
        ));
        item.setItemMeta(meta);
        return item;
    }

    private ServerSwitchPacket createServerSwitchPacket(Player player, ServerInfo serverInfo) {
        ServerSwitchPacket packet = new ServerSwitchPacket();
        packet.setPlayerUniqueId(player.getUniqueId());
        packet.setTargetServerId(serverInfo.id());
        return packet;
    }

    private TransferPlayerPacket createTransferPacket(Player player, String targetId, String requestingId) throws IOException {
        TransferPlayerPacket packet = new TransferPlayerPacket();

        packet.setServerTarget(targetId);
        packet.setRequestingServer(requestingId);
        packet.setPlayerUniqueId(player.getUniqueId());
        packet.setNickname(player.getName());
        packet.setGameMode(player.getGameMode().name());
        packet.setInventory(SerdeHelper.serializeItems(player.getInventory().getContents()));
        packet.setArmor(SerdeHelper.serializeItems(player.getInventory().getArmorContents()));
        packet.setEnderChest(SerdeHelper.serializeItems(player.getEnderChest().getContents()));
        packet.setPotionEffects(SerdeHelper.serializePotionEffects(player.getActivePotionEffects()));
        packet.setLocation(SerdeHelper.serializeLocation(player.getLocation()));
        packet.setHealth(player.getHealth());
        packet.setWalkSpeed(player.getWalkSpeed());
        packet.setFlySpeed(player.getFlySpeed());
        packet.setExperience(player.getExp());
        packet.setTotalExperience(player.getTotalExperience());
        packet.setLevel(player.getLevel());
        packet.setFoodLevel(player.getFoodLevel());
        packet.setFireTicks(player.getFireTicks());
        packet.setRemainingAir(player.getRemainingAir());
        packet.setHeldSlot(player.getInventory().getHeldItemSlot());
        packet.setAllowFlight(player.getAllowFlight());
        packet.setFlying(player.isFlying());
        packet.setOp(player.isOp());
        packet.setSprinting(player.isSprinting());
        packet.setSneaking(player.isSneaking());

        return packet;
    }
}
