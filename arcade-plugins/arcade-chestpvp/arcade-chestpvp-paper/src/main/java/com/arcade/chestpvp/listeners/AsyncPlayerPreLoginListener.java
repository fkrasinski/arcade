package com.arcade.chestpvp.listeners;

import com.arcade.chestpvp.Caches;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

public class AsyncPlayerPreLoginListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onPreLogin(AsyncPlayerPreLoginEvent event) {
        if (Caches.PLAYER_DATA_CACHE.getIfPresent(event.getUniqueId()) == null) {
//            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, Component.text("MISSING DATA", NamedTextColor.RED));
        }
    }
}
