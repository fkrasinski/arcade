package com.arcade.chestpvp.handler;

import com.arcade.base.packet.PacketHandler;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.chestpvp.Caches;
import com.arcade.model.Channels;
import com.arcade.model.packet.AckPacket;
import com.arcade.model.packet.Packet;
import com.arcade.model.packet.TransferPlayerPacket;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransferPlayerPacketHandler implements PacketHandler<TransferPlayerPacket> {
    private static final Logger logger = LoggerFactory.getLogger(TransferPlayerPacketHandler.class);

    private final RedisPubSubPacket redisPubSub;

    @Inject
    public TransferPlayerPacketHandler(RedisPubSubPacket redisPubSub) {
        this.redisPubSub = redisPubSub;
    }

    @Override
    public void handle(TransferPlayerPacket packet) {
        Caches.PLAYER_DATA_CACHE.put(packet.getPlayerUniqueId(), packet);
        Packet ack = new AckPacket();
        ack.setCallbackId(packet.getCallbackId());
        redisPubSub.publish(Channels.server(packet.getRequestingServer()), ack);
    }
}
