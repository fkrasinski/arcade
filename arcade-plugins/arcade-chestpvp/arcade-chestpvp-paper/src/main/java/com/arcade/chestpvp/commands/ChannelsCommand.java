package com.arcade.chestpvp.commands;

import cloud.commandframework.Command;
import cloud.commandframework.CommandManager;
import com.arcade.chestpvp.inventory.ChannelsInventory;
import com.arcade.model.ServerInfo;
import com.arcade.model.ServerType;
import com.arcade.redis.RedisKeys;
import com.arcade.redis.RedisRepositoryAsync;
import com.google.inject.Inject;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ChannelsCommand {
    private static final Logger logger = LoggerFactory.getLogger(ChannelsCommand.class);

    private final RedisRepositoryAsync redisRepository;
    private final CommandManager<CommandSender> commandManager;
    private final ChannelsInventory channelsInventory;
    private final JavaPlugin javaPlugin;

    @Inject
    public ChannelsCommand(RedisRepositoryAsync redisRepository,
                           CommandManager<CommandSender> commandManager,
                           ChannelsInventory channelsInventory,
                           JavaPlugin javaPlugin) {
        this.redisRepository = redisRepository;
        this.commandManager = commandManager;
        this.channelsInventory = channelsInventory;
        this.javaPlugin = javaPlugin;
    }

    public Command<CommandSender> createCommand() {
        return commandManager.commandBuilder("channels", "ch")
                             .handler(ctx -> redisRepository.getAllByKey(RedisKeys.serversKey(), ServerInfo.class)
                                                            .thenApply(this::filterServers)
                                                            .thenAccept(servers -> createAndOpenInventory(ctx.getSender(), servers))
                                                            .orTimeout(3, TimeUnit.SECONDS)
                                                            .exceptionally(ex -> {
                                                                logger.error("Error occurred when creating channels inventory", ex);
                                                                ctx.getSender().sendMessage(Component.text("Error occurred, try again later", NamedTextColor.RED));
                                                                return null;
                                                            }))
                             .build();
    }

    private List<ServerInfo> filterServers(Map<String, ServerInfo> servers) {
        return servers.values()
                      .stream()
                      .filter(info -> ServerType.CHEST_PVP == info.serverType())
                      .toList();
    }

    private void createAndOpenInventory(CommandSender sender, List<ServerInfo> servers) {
        var inventory = channelsInventory.create(servers);
        // must run in main thread
        Bukkit.getScheduler().runTask(javaPlugin, () -> inventory.openInventory((Player) sender));
    }
}
