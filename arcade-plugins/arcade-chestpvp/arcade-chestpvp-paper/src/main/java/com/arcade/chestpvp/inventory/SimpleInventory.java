package com.arcade.chestpvp.inventory;

import net.kyori.adventure.text.Component;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class SimpleInventory implements Listener {

    private final Inventory inventory;
    private Consumer<InventoryDragEvent> dragAction;
    private Consumer<InventoryCloseEvent> closeAction;
    private Consumer<InventoryOpenEvent> openAction;
    private Consumer<InventoryClickEvent> clickAction;
    private final Map<Integer, Consumer<InventoryClickEvent>> clickActions;

    public SimpleInventory(Plugin plugin, Component title, int slots) {
        this.inventory = Bukkit.createInventory(null, slots, title);
        this.clickActions = new HashMap<>();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void setItem(int slot, ItemStack itemStack, Consumer<InventoryClickEvent> clickAction) {
        Validate.isTrue(slot < this.inventory.getSize(), "Overflowed slot, got " + slot + ", expected 0 - " + this.inventory.getSize());
        Validate.notNull(clickAction);
        this.inventory.setItem(slot, itemStack);
        this.clickActions.put(slot, clickAction);
    }

    public void setItem(ItemStack itemStack) {
        Validate.isTrue(this.inventory.firstEmpty() != -1, "There is no free slot to setItem");
        this.inventory.setItem(this.inventory.firstEmpty(), itemStack);
    }

    public void setLastSlotItem(ItemStack itemStack) {
        this.inventory.setItem(this.inventory.getSize() - 1, itemStack);
    }

    public void setItem(int slot, ItemStack itemStack) {
        Validate.isTrue(slot < this.inventory.getSize(), "Overflowed slot, got " + slot + ", expected 0 - " + this.inventory.getSize());
        this.inventory.setItem(slot, itemStack);
    }

    public SimpleInventory fill(ItemStack itemStack) {
        for (int i = 0; i < this.inventory.getSize(); ++i) {
            this.inventory.setItem(i, itemStack);
        }
        return this;
    }

    public SimpleInventory addItems(Collection<ItemStack> items) {
        int i = 0;
        for (ItemStack is : items) {
            this.inventory.setItem(i, is);
            ++i;
        }
        return this;
    }

    public SimpleInventory addItems(ItemStack[] items) {
        for (int i = 0; i < items.length; ++i) {
            this.inventory.setItem(i, items[i]);
        }
        return this;
    }

    public SimpleInventory addItems(List<ItemStack> items) {
        for (int i = 0; i < items.size(); ++i) {
            this.inventory.setItem(i, items.get(i));
        }
        return this;
    }

    public ItemStack getItem(int i) {
        return this.inventory.getItem(i);
    }

    public SimpleInventory setOpenAction(Consumer<InventoryOpenEvent> action) {
        this.openAction = action;
        return this;
    }

    public SimpleInventory setClickAction(Consumer<InventoryClickEvent> action) {
        this.clickAction = action;
        return this;
    }

    public SimpleInventory setCloseAction(Consumer<InventoryCloseEvent> action) {
        this.closeAction = action;
        return this;
    }

    public SimpleInventory setDragAction(Consumer<InventoryDragEvent> action) {
        this.dragAction = action;
        return this;
    }

    public void openInventory(Player player) {
        player.openInventory(this.inventory);
    }

    public void openInventory(HumanEntity player) {
        player.openInventory(this.inventory);
    }

    public SimpleInventory openInventory(Player[] players) {
        for (Player player : players) {
            this.openInventory(player);
        }
        return this;
    }

    public SimpleInventory openInventory(Collection<? extends Player> players) {
        for (Player player : players) {
            this.openInventory(player);
        }
        return this;
    }

    public Inventory get() {
        return this.inventory;
    }

    @EventHandler
    private void onInventoryClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        if (this.clickAction != null) {
            this.clickAction.accept(event);
        }
        event.setCancelled(true);
        if (!this.clickActions.containsKey(event.getRawSlot())) {
            return;
        }
        final Consumer<InventoryClickEvent> action = this.clickActions.get(event.getRawSlot());
        action.accept(event);
    }

    @EventHandler
    private void onInventoryOpen(InventoryOpenEvent event) {
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        if (this.openAction != null) {
            this.openAction.accept(event);
        }
    }

    @EventHandler
    private void onInventoryClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        if (this.closeAction != null) {
            this.closeAction.accept(event);
        }
    }

    @EventHandler
    private void onInventoryMoveItemEvent(InventoryMoveItemEvent e) {
        if (e.getDestination().equals(this.inventory)) {
            e.setCancelled(true);
            return;
        }
        if (e.getInitiator().equals(this.inventory)) {
            e.setCancelled(true);
            return;
        }
        if (this.inventory.equals(e.getSource())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onInventoryInteractEvent(InventoryInteractEvent e) {
        if (this.inventory.equals(e.getInventory())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onInventoryDragEvent(InventoryDragEvent e) {
        if (!e.getInventory().equals(this.inventory)) {
            return;
        }
        if (this.dragAction == null) {
            e.setCancelled(true);
            return;
        }
        this.dragAction.accept(e);
    }
}
