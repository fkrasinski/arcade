package com.arcade.chestpvp;

import cloud.commandframework.CommandManager;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class BasicModule extends AbstractModule {

    private final JavaPlugin javaPlugin;
    private final CommandManager<CommandSender> commandManager;

    public BasicModule(JavaPlugin javaPlugin, CommandManager<CommandSender> commandManager) {
        this.javaPlugin = javaPlugin;
        this.commandManager = commandManager;
    }

    @Override
    protected void configure() {

    }

    @Provides
    public JavaPlugin plugin() {
        return javaPlugin;
    }

    @Provides
    public CommandManager<CommandSender> commandManager() {
        return commandManager;
    }
}
