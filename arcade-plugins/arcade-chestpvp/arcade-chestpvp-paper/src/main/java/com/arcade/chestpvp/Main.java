package com.arcade.chestpvp;

import cloud.commandframework.CommandManager;
import cloud.commandframework.execution.CommandExecutionCoordinator;
import cloud.commandframework.paper.PaperCommandManager;
import com.arcade.base.packet.PacketHandlerRegistry;
import com.arcade.chestpvp.commands.ChannelsCommand;
import com.arcade.chestpvp.handler.TransferPlayerPacketHandler;
import com.arcade.chestpvp.listeners.AsyncPlayerPreLoginListener;
import com.arcade.chestpvp.listeners.PlayerJoinListener;
import com.arcade.model.packet.TransferPlayerPacket;
import com.arcade.paper.base.ArcadePlugin;
import com.google.inject.Injector;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.function.Function;

public class Main extends JavaPlugin implements Listener {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private ArcadePlugin arcadeBasePlugin;
    private Injector injector;

    @Override
    public void onEnable() {
        this.arcadeBasePlugin = (ArcadePlugin) Bukkit.getServer().getPluginManager().getPlugin("ArcadeBase");
        if (Objects.isNull(arcadeBasePlugin)) {
            logger.error("Couldn't find base arcade plugin.");
            Bukkit.getServer().shutdown();
            return;
        }
        var commandManager = createCommandManager();
        this.injector = arcadeBasePlugin.getBaseInjector().createChildInjector(new BasicModule(this, commandManager));

        registerHandlers();
        registerListeners();
        registerCommands(commandManager);
    }


    private void registerHandlers() {
        var registry = injector.getInstance(PacketHandlerRegistry.class);
        registry.addHandler(TransferPlayerPacket.class, injector.getInstance(TransferPlayerPacketHandler.class));
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(injector.getInstance(AsyncPlayerPreLoginListener.class), this);
        getServer().getPluginManager().registerEvents(injector.getInstance(PlayerJoinListener.class), this);
    }

    private void registerCommands(CommandManager<CommandSender> commandManager) {
        commandManager.command(injector.getInstance(ChannelsCommand.class).createCommand());
    }

    private CommandManager<CommandSender> createCommandManager() {
        try {
            return new PaperCommandManager<>(
                    this,
                    CommandExecutionCoordinator.simpleCoordinator(),
                    Function.identity(),
                    Function.identity()
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
