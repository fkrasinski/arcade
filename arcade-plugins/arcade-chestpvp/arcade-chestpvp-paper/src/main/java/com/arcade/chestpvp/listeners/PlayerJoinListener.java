package com.arcade.chestpvp.listeners;

import com.arcade.chestpvp.Caches;
import com.arcade.model.packet.TransferPlayerPacket;
import com.arcade.paper.base.helper.SerdeHelper;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class PlayerJoinListener implements Listener {
    private static final Logger logger = LoggerFactory.getLogger(PlayerJoinListener.class);

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.getInventory().setArmorContents(null);
        player.getInventory().setArmorContents(null);
        player.getActivePotionEffects().clear();

        TransferPlayerPacket data = Caches.PLAYER_DATA_CACHE.getIfPresent(player.getUniqueId());

        if (data == null) {
            return;
        }
        try {
            assignData(player, data);
        } catch (IOException e) {
            logger.error("Failed to assign data", e);
        }

    }

    private static void assignData(Player player, TransferPlayerPacket data) throws IOException {
        player.setGameMode(GameMode.valueOf(data.getGameMode()));
        player.getInventory().setContents(SerdeHelper.deserializeItems(data.getInventory()));
        player.getInventory().setArmorContents(SerdeHelper.deserializeItems(data.getArmor()));
        player.getEnderChest().setContents(SerdeHelper.deserializeItems(data.getEnderChest()));
        player.addPotionEffects(SerdeHelper.deserializePotionEffects(data.getPotionEffects()));
        player.setHealth(data.getHealth());
        player.setWalkSpeed(data.getWalkSpeed());
        player.setFlySpeed(data.getFlySpeed());
        player.setExp(data.getExperience());
        player.setTotalExperience(data.getTotalExperience());
        player.setLevel(data.getLevel());
        player.setFoodLevel(data.getFoodLevel());
        player.setFireTicks(data.getFireTicks());
        player.setRemainingAir(data.getRemainingAir());
        player.getInventory().setHeldItemSlot(data.getHeldSlot());
        player.setAllowFlight(data.isAllowFlight());
        player.setOp(data.isOp());
        player.setSprinting(data.isSprinting());
        player.setSneaking(data.isSneaking());
        player.teleport(SerdeHelper.deserializeLocation(data.getLocation()).toLocation(Bukkit.getWorlds().get(0)));
    }
}
