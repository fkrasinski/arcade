package com.arcade.chestpvp;

import com.arcade.model.packet.TransferPlayerPacket;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.time.Duration;
import java.util.UUID;

public class Caches {

    public static final Cache<UUID, TransferPlayerPacket> PLAYER_DATA_CACHE = CacheBuilder.newBuilder()
                                                                              .expireAfterWrite(Duration.ofSeconds(30))
                                                                              .build();
}
