package com.arcade.model;

import com.arcade.proxy.base.ArcadePlugin;
import com.google.inject.Injector;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Dependency;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import org.slf4j.Logger;

import javax.inject.Inject;

@Plugin(id = "arcade-chestpvp", dependencies = { @Dependency(id = "arcade-base") })
public class Main {

    private final Logger logger;
    private final ProxyServer server;
    private Injector injector;
    private ArcadePlugin arcadeBasePlugin;

    @Inject
    public Main(Logger logger, ProxyServer server) {
        this.logger = logger;
        this.server = server;
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        logger.info("Initialized proxy");
        this.arcadeBasePlugin = (ArcadePlugin) server.getPluginManager().getPlugin("arcade-base").get().getInstance().get();
        this.injector = arcadeBasePlugin.getBaseInjector().createChildInjector(new BasicModule(server));
    }

}
