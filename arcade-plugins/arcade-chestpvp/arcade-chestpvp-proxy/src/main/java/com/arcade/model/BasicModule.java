package com.arcade.model;

import com.google.inject.AbstractModule;
import com.velocitypowered.api.proxy.ProxyServer;

public class BasicModule extends AbstractModule {

    private final ProxyServer proxyServer;

    public BasicModule(ProxyServer proxyServer) {
        this.proxyServer = proxyServer;
    }

    @Override
    protected void configure() {

    }
}
