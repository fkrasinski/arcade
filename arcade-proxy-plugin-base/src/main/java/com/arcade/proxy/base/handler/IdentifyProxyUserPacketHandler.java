package com.arcade.proxy.base.handler;

import com.arcade.base.packet.PacketHandler;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.model.Channels;
import com.arcade.model.packet.IdentifyProxyUserRequestPacket;
import com.arcade.model.packet.IdentifyProxyUserResponsePacket;
import com.arcade.proxy.base.ArcadePlugin;
import com.google.inject.Inject;
import com.velocitypowered.api.proxy.ProxyServer;

//todo: that packet shouldn't exist and it should be done better but it's easier for now to leave it like this
public class IdentifyProxyUserPacketHandler implements PacketHandler<IdentifyProxyUserRequestPacket> {

    private final ArcadePlugin arcadePlugin;
    private final ProxyServer proxyServer;
    private final RedisPubSubPacket redisPubSubPacket;

    @Inject
    public IdentifyProxyUserPacketHandler(ArcadePlugin arcadePlugin,
                                          ProxyServer proxyServer,
                                          RedisPubSubPacket redisPubSubPacket) {
        this.arcadePlugin = arcadePlugin;
        this.proxyServer = proxyServer;
        this.redisPubSubPacket = redisPubSubPacket;
    }


    @Override
    public void handle(IdentifyProxyUserRequestPacket packet) {
        proxyServer.getPlayer(packet.getUserUniqueId())
                   .ifPresent(player -> {
                       var response = new IdentifyProxyUserResponsePacket();
                       response.setCallbackId(packet.getCallbackId());
                       response.setUserUniqueId(packet.getUserUniqueId());
                       response.setProxyUniqueId(arcadePlugin.getServerId());
                       redisPubSubPacket.publish(Channels.server(packet.getRequestingServerUniqueId()), response);
                   });
    }
}
