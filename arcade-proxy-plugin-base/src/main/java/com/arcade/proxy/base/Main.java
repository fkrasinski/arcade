package com.arcade.proxy.base;

import com.arcade.base.InMemoryRepository;
import com.arcade.base.packet.Callback;
import com.arcade.base.packet.PacketHandlerRegistry;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.model.Channels;
import com.arcade.model.ServerInfo;
import com.arcade.model.ServerType;
import com.arcade.model.packet.*;
import com.arcade.proxy.base.handler.IdentifyProxyUserPacketHandler;
import com.arcade.proxy.base.handler.ServerRegisteredPacketHandler;
import com.arcade.proxy.base.handler.ServerSwitchPacketHandler;
import com.arcade.proxy.base.handler.ServerUnregisterPacketHandler;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.PlayerChooseInitialServerEvent;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Plugin(id = "arcade-base")
public class Main implements ArcadePlugin {

    private final Logger logger;
    private final ProxyServer server;
    private final Injector injector;
    private final String serverId;
    private final InMemoryRepository<String, ServerInfo> serversStorage;

    @Inject
    public Main(Logger logger, ProxyServer server) {
        this.logger = logger;
        this.server = server;
        this.injector = Guice.createInjector(new ProxyPluginModule(server, this));
        this.serverId = UUID.randomUUID().toString();
        this.serversStorage = new InMemoryRepository<>();
    }

    @Override
    public Injector getBaseInjector() {
        return injector;
    }

    @Override
    public String getServerId() {
        return serverId;
    }

    @Override
    public InMemoryRepository<String, ServerInfo> getServersStorage() {
        return serversStorage;
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        registerHandlers();
        tellMaster();
        keepAlive();
        logger.info("Initialized proxy");
    }

    //todo: remove
    @Subscribe
    public void onInitialConnect(PlayerChooseInitialServerEvent event) {
        server.getAllServers()
              .stream()
              .findFirst()
              .ifPresent(event::setInitialServer);
    }


    private void registerHandlers() {
        var registry = injector.getInstance(PacketHandlerRegistry.class);
        registry.addHandler(IdentifyProxyUserRequestPacket.class, injector.getInstance(IdentifyProxyUserPacketHandler.class));
        registry.addHandler(ServerRegisteredPacket.class, injector.getInstance(ServerRegisteredPacketHandler.class));
        registry.addHandler(ServerUnregisterPacket.class, injector.getInstance(ServerUnregisterPacketHandler.class));
        registry.addHandler(ServerSwitchPacket.class, injector.getInstance(ServerSwitchPacketHandler.class));
    }


    private void tellMaster() {
        RedisPubSubPacket pubSub = injector.getInstance(RedisPubSubPacket.class);
        CompletableFuture<Void> latch = new CompletableFuture<>();

        // specific server channel
        pubSub.subscribe(Channels.PROXIES);
        pubSub.subscribe(Channels.proxy(getServerId()));

        logger.info("Telling master about new server");
        pubSub.publishWithCallback(Channels.MASTER, serverRegitrationPacket(),
                (Callback<ServerRegisteredPacket>) response -> {
                    logger.info("Got confirmation from master");
                    latch.complete(null);
                });

        try {
            logger.info("Waiting for confirmation from master");
            latch.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("Didn't receive confirmation from master in 10 seconds", e);
            server.shutdown();
        }
    }

    private void keepAlive() {
        RedisPubSubPacket pubSub = injector.getInstance(RedisPubSubPacket.class);
        server.getScheduler()
              .buildTask(this, () -> {
                  Packet packet = new HeartbeatPacket(getServerId(), System.currentTimeMillis());
                  pubSub.publish(Channels.MASTER, packet);
              })
              .repeat(1L, TimeUnit.SECONDS)
              .schedule();
    }

    private Packet serverRegitrationPacket() {
        return new ServerRegisterRequestPacket(new ServerInfo(
                getServerId(),
                ServerType.PROXY,
                "127.0.0.1",
                server.getConfiguration().getQueryPort(),
                server.getPlayerCount(),
                0,
                System.currentTimeMillis()));
    }
}
