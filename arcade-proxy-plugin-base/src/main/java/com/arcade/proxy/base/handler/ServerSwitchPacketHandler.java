package com.arcade.proxy.base.handler;

import com.arcade.base.packet.PacketHandler;
import com.arcade.model.packet.ServerSwitchPacket;
import com.google.inject.Inject;
import com.velocitypowered.api.proxy.ProxyServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerSwitchPacketHandler implements PacketHandler<ServerSwitchPacket> {
    private static final Logger logger = LoggerFactory.getLogger(ServerRegisteredPacketHandler.class);

    private final ProxyServer proxyServer;

    @Inject
    public ServerSwitchPacketHandler(ProxyServer proxyServer) {
        this.proxyServer = proxyServer;
    }

    @Override
    public void handle(ServerSwitchPacket packet) {
        var maybeServer = proxyServer.getAllServers()
                                     .stream()
                                     .filter(server -> server.getServerInfo().getName().equals(packet.getTargetServerId()))
                                     .findFirst();

        if (maybeServer.isEmpty()) {
            logger.warn("Tried to switch to server {} that is not registered.", packet.getTargetServerId());
            return;
        }

        var maybePlayer = proxyServer.getPlayer(packet.getPlayerUniqueId());

        if (maybePlayer.isEmpty()) {
            logger.warn("Tried to transfer player {} to server {} but player isn't online.", packet.getPlayerUniqueId(), packet.getTargetServerId());
            return;
        }

        maybePlayer.get().createConnectionRequest(maybeServer.get()).fireAndForget();
    }
}
