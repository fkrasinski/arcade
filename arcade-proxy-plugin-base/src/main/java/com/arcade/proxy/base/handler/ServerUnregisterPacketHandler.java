package com.arcade.proxy.base.handler;

import com.arcade.base.packet.PacketHandler;
import com.arcade.model.packet.ServerUnregisterPacket;
import com.arcade.proxy.base.ArcadePlugin;
import com.google.inject.Inject;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerUnregisterPacketHandler implements PacketHandler<ServerUnregisterPacket> {
    private static final Logger logger = LoggerFactory.getLogger(ServerUnregisterPacketHandler.class);

    private final ArcadePlugin arcadePlugin;
    private final ProxyServer proxyServer;

    @Inject
    public ServerUnregisterPacketHandler(ArcadePlugin arcadePlugin,
                                         ProxyServer proxyServer) {
        this.arcadePlugin = arcadePlugin;
        this.proxyServer = proxyServer;
    }

    @Override
    public void handle(ServerUnregisterPacket packet) {
        var server = proxyServer.getAllServers()
                                .stream()
                                .map(RegisteredServer::getServerInfo)
                                .filter(serverInfo -> serverInfo.getName().equals(packet.getId()))
                                .findFirst();
        server.ifPresent(s -> {
            logger.info("Unregistering server: {}", s);
            proxyServer.unregisterServer(s);
            arcadePlugin.getServersStorage().remove(packet.getId());
        });
    }
}
