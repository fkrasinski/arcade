package com.arcade.proxy.base;

import com.arcade.base.InMemoryRepository;
import com.arcade.model.ServerInfo;
import com.google.inject.Injector;

public interface ArcadePlugin {

    Injector getBaseInjector();

    String getServerId();

    InMemoryRepository<String, ServerInfo> getServersStorage();

}
