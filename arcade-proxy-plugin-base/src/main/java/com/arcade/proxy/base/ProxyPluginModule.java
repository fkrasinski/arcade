package com.arcade.proxy.base;

import com.arcade.base.BasePluginModule;
import com.arcade.redis.RedisConfig;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.velocitypowered.api.proxy.ProxyServer;

import java.time.Duration;

public class ProxyPluginModule extends AbstractModule {

    private final ProxyServer proxyServer;
    private final ArcadePlugin arcadePlugin;

    public ProxyPluginModule(ProxyServer proxyServer, ArcadePlugin arcadePlugin) {
        this.proxyServer = proxyServer;
        this.arcadePlugin = arcadePlugin;
    }

    @Override
    protected void configure() {
        install(new BasePluginModule());

        bindConstant().annotatedWith(Names.named("redis.config.nodeURI")).to("redis://127.0.0.1:6379");
        bindConstant().annotatedWith(Names.named("redis.config.token")).to("");
    }


    @Provides
    @Named("redis.config.timeout")
    public Duration provideTimeout() {
        return Duration.ofSeconds(10);
    }

    @Provides
    public ProxyServer proxyServer() {
        return proxyServer;
    }

    @Provides
    public ArcadePlugin arcadePlugin() {
        return arcadePlugin;
    }
}
