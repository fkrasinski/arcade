package com.arcade.proxy.base.handler;

import com.arcade.base.packet.PacketHandler;
import com.arcade.model.ServerType;
import com.arcade.model.packet.ServerRegisteredPacket;
import com.arcade.proxy.base.ArcadePlugin;
import com.google.inject.Inject;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.ServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class ServerRegisteredPacketHandler implements PacketHandler<ServerRegisteredPacket> {
    private static final Logger logger = LoggerFactory.getLogger(ServerRegisteredPacketHandler.class);

    private final ArcadePlugin arcadePlugin;
    private final ProxyServer proxyServer;

    @Inject
    public ServerRegisteredPacketHandler(ArcadePlugin arcadePlugin,
                                         ProxyServer proxyServer) {
        this.arcadePlugin = arcadePlugin;
        this.proxyServer = proxyServer;
    }

    @Override
    public void handle(ServerRegisteredPacket packet) {
        var serverInfo = packet.getServerInfo();
        if (ServerType.PROXY == packet.getServerInfo().serverType()) {
            return;
        }
        logger.info("Registering server: {}", packet);
        arcadePlugin.getServersStorage().put(serverInfo.id(), serverInfo);
        proxyServer.registerServer(new ServerInfo(serverInfo.id(), new InetSocketAddress(serverInfo.host(), serverInfo.port())));
    }
}
