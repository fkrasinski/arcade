package com.arcade.redis;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class RedisRepositoryAsyncImpl implements RedisRepositoryAsync {

    private final RedisRepository delegate;

    public RedisRepositoryAsyncImpl(RedisRepository delegate) {
        this.delegate = delegate;
    }

    @Override
    public <T> CompletableFuture<T> get(String key, Class<T> clazz) {
        return delegate.get(key, clazz).toFuture();
    }

    @Override
    public <T> CompletableFuture<T> get(String key, String hashKey, Class<T> clazz) {
        return delegate.get(key, hashKey, clazz).toFuture();
    }

    @Override
    public CompletableFuture<Void> set(String key, Object value) {
        return delegate.set(key, value).toFuture();
    }

    @Override
    public CompletableFuture<Void> set(String key, String hashKey, Object value) {
        return delegate.set(key, hashKey, value).toFuture();
    }

    @Override
    public CompletableFuture<Void> set(String key, Object value, Duration duration) {
        return delegate.set(key, value, duration).toFuture();
    }

    @Override
    public CompletableFuture<Boolean> hasKey(String key) {
        return delegate.hasKey(key).toFuture();
    }

    @Override
    public CompletableFuture<Boolean> hasKey(String key, String hashKey) {
        return delegate.hasKey(key, hashKey).toFuture();
    }

    @Override
    public <T> CompletableFuture<Map<String, T>> getAllByKey(String key, Class<T> clazz) {
        return delegate.getAllByKey(key, clazz).toFuture();
    }

    @Override
    public CompletableFuture<Void> delete(String key) {
        return delegate.delete(key).toFuture();
    }

    @Override
    public CompletableFuture<Void> delete(String key, String hashKey) {
        return delegate.delete(key, hashKey).toFuture();
    }
}
