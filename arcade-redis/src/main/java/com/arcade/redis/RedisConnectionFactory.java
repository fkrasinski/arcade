package com.arcade.redis;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.codec.ByteArrayCodec;
import io.lettuce.core.codec.RedisCodec;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedisConnectionFactory {
    private static final Logger logger = LoggerFactory.getLogger(RedisConnectionFactory.class);

    private final RedisConfig redisConfig;
    private final RedisClient redisClient;

    public RedisConnectionFactory(RedisConfig redisConfig) {
        this.redisConfig = redisConfig;
        this.redisClient = createSingleClient();
    }

    public StatefulRedisConnection<String, byte[]> createConnection() {
        var connection = redisClient.connect(RedisCodec.of(new StringCodec(), new ByteArrayCodec()));
        connection.setTimeout(redisConfig.getTimeout());
        return connection;
    }

    public StatefulRedisPubSubConnection<String, byte[]> createPubSubConnection() {
        var connection = redisClient.connectPubSub(RedisCodec.of(new StringCodec(), new ByteArrayCodec()));
        connection.setTimeout(redisConfig.getTimeout());
        return connection;
    }

    private RedisClient createSingleClient() {
        return RedisClient.create(redisConfig.getRedisURI());
    }
}
