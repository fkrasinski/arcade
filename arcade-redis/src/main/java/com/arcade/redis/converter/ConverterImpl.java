package com.arcade.redis.converter;

import com.arcade.redis.exception.ConverterException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ConverterImpl implements Converter {
    private static final Logger logger = LoggerFactory.getLogger(ConverterImpl.class);

    private final boolean writeJson;
    private final ObjectMapper objectMapper;

    public ConverterImpl(boolean writeJson) {
        this.writeJson = writeJson;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    @Override
    public <T> T fromBytes(String id, byte[] data, Class<T> clazz) {
        try {
            return isJson(data)
                    ? objectMapper.readValue(data, clazz)
                    : null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] toBytes(String id, Object object) {
        try {
            return writeJson
                    ? objectMapper.writeValueAsBytes(object)
                    : null;
        } catch (Exception e) {
            logger.error("Error", e);
            throw new ConverterException(e, id);
        }
    }

    private boolean isJson(byte[] bytes) {
        if (bytes.length > 0) {
            byte firstByte = bytes[0];
            byte lastByte = bytes[bytes.length - 1];
            return isJsonObject(firstByte, lastByte) || isJsonArray(firstByte, lastByte);
        }
        return false;
    }

    private boolean isJsonObject(byte firstByte, byte lastByte) {
        return firstByte == (byte) '{' && lastByte == (byte) '}';
    }

    private boolean isJsonArray(byte firstByte, byte lastByte) {
        return firstByte == (byte) '[' && lastByte == (byte) ']';
    }
}
