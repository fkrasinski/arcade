package com.arcade.redis.converter;

public interface Converter {

    <T> T fromBytes(String id, byte[] data, Class<T> clazz);

    byte[] toBytes(String id, Object object);

}
