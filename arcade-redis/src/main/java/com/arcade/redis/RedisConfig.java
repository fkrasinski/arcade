package com.arcade.redis;

import io.lettuce.core.RedisURI;

import java.time.Duration;

public class RedisConfig {

    private final RedisURI redisURI;
    private final String password;
    private final Duration timeout;

    public RedisConfig(String nodeURI, String password, Duration timeout) {
        this.password = password;
        this.redisURI = extractURI(nodeURI);
        this.timeout = timeout;
    }

    private RedisURI extractURI(String uri) {
        RedisURI redisURI = RedisURI.create(uri);
        if (!password.isBlank()) {
            redisURI.setPassword(password.toCharArray());
        }
        return redisURI;
    }

    public RedisURI getRedisURI() {
        return redisURI;
    }

    public String getPassword() {
        return password;
    }

    public Duration getTimeout() {
        return timeout;
    }
}
