package com.arcade.redis;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface RedisRepositoryAsync {

    <T> CompletableFuture<T> get(String key, Class<T> clazz);

    <T> CompletableFuture<T> get(String key, String hashKey, Class<T> clazz);

    CompletableFuture<Void> set(String key, Object value);

    CompletableFuture<Void> set(String key, String hashKey, Object value);

    CompletableFuture<Void> set(String key, Object value, Duration duration);

    CompletableFuture<Boolean> hasKey(String key);

    CompletableFuture<Boolean> hasKey(String key, String hashKey);

    <T> CompletableFuture<Map<String, T>> getAllByKey(String key, Class<T> clazz);

    CompletableFuture<Void> delete(String key);

    CompletableFuture<Void> delete(String key, String hashKey);

}
