package com.arcade.redis;

import com.arcade.redis.converter.Converter;
import com.arcade.redis.exception.ConverterException;
import io.lettuce.core.KeyValue;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RedisRepositoryImpl implements RedisRepository {
    private static final Logger logger = LoggerFactory.getLogger(RedisRepositoryImpl.class);

    private final StatefulRedisConnection<String, byte[]> connection;
    private final RedisReactiveCommands<String, byte[]> commands;
    private final Converter converter;

    public RedisRepositoryImpl(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        this.connection = redisConnectionFactory.createConnection();
        this.commands = connection.reactive();
        this.converter = converter;
        Runtime.getRuntime().addShutdownHook(new Thread(this::cleanUp)); //todo: IDK?
    }

    @Override
    public <T> Mono<T> get(String key, Class<T> clazz) {
        return commands.get(key)
                       .map(value -> converter.fromBytes(key, value, clazz))
                       .doOnNext(value -> logger.debug("Get from cache key {} value {}", key, value))
                       .onErrorResume(ConverterException.class, ex -> handleInvalidKey(key, ex));
    }

    @Override
    public <T> Mono<T> get(String key, String hashKey, Class<T> clazz) {
        return commands.hget(key, hashKey)
                       .map(value -> converter.fromBytes(hashKey, value, clazz))
                       .doOnNext(value -> logger.debug("Get from cache key {} hashKey {} value {}", key, hashKey, value))
                       .onErrorResume(ConverterException.class, ex -> handleInvalidHashKey(key, ex.getId(), ex));
    }

    @Override
    public Mono<Void> set(String key, Object value) {
        return Mono.just(converter.toBytes(key, value))
                   .doOnNext(__ -> logger.debug("Setting value in cache for key {} value {}", key, value))
                   .flatMap(converted -> commands.set(key, converted))
                   .then();
    }

    @Override
    public Mono<Void> set(String key, Object value, Duration duration) {
        return Mono.just(converter.toBytes(key, value))
                   .doOnNext(__ -> logger.debug("Setting value in cache for key {} value {} with TTL {}s", key, value, duration.getSeconds()))
                   .flatMap(converted -> commands.setex(key, duration.toSeconds(), converted))
                   .then();
    }

    @Override
    public Mono<Void> set(String key, String hashKey, Object value) {
        return Mono.just(converter.toBytes(key, value))
                   .doOnNext(__ -> logger.debug("Setting value in cache for key {} hashKey {} value {}", key, hashKey, value))
                   .flatMap(converted -> commands.hset(key, hashKey, converted))
                   .then();
    }

    @Override
    public Mono<Boolean> hasKey(String key) {
        return commands.exists(key)
                       .map(this::convertExistenceStatus);
    }

    @Override
    public Mono<Boolean> hasKey(String key, String hashKey) {
        return commands.hexists(key, hashKey);
    }

    @Override
    public <T> Mono<Map<String, T>> getAllByKey(String key, Class<T> clazz) {
        return commands.hgetall(key)
                       .filter(KeyValue::hasValue)
                       .collect(Collectors.toMap(KeyValue::getKey, item -> converter.fromBytes(item.getKey(), item.getValue(), clazz)))
                       .filter(Predicate.not(Map::isEmpty))
                       .onErrorResume(ConverterException.class, ex -> handleInvalidHashKey(key, ex.getId(), ex));
    }

    @Override
    public Mono<Void> delete(String key) {
        return commands.del(key)
                       .doOnSuccess(__ -> logger.debug("Removed value for key {}", key))
                       .then();
    }

    @Override
    public Mono<Void> delete(String key, String hashKey) {
        return commands.hdel(key, hashKey)
                       .doOnSuccess(__ -> logger.debug("Removed value for key {} hashKey {}", key, hashKey))
                       .then();
    }

    private <T> Mono<T> handleInvalidKey(String key, ConverterException exception) {
        logger.error("Cached value for id: {} was invalid", key);
        return delete(key).then(Mono.error(exception));
    }

    private <T> Mono<T> handleInvalidHashKey(String key, String hashKey, ConverterException exception) {
        logger.error("Cached value for id: {} was invalid", key);
        return delete(key, hashKey).then(Mono.error(exception));
    }

    private boolean convertExistenceStatus(long value) {
        return value > 0;
    }

    private void cleanUp() {
        connection.close();
    }
}
