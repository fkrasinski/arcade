package com.arcade.redis.exception;

public class ConverterException extends RuntimeException {

    private final String id;

    public ConverterException(Exception e, String id) {
        super(e);
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
