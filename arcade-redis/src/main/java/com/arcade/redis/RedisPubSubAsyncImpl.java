package com.arcade.redis;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class RedisPubSubAsyncImpl implements RedisPubSubAsync {

    private final RedisPubSub delegate;

    public RedisPubSubAsyncImpl(RedisPubSub delegate) {
        this.delegate = delegate;
    }

    @Override
    public CompletableFuture<Void> subscribe(String channel, Consumer<PubSubMessage> handler) {
        return delegate.subscribe(channel, handler).toFuture();
    }

    @Override
    public CompletableFuture<Void> unsubscribe(String channel) {
        return delegate.unsubscribe(channel).toFuture();
    }

    @Override
    public CompletableFuture<Void> patternSubscribe(String pattern, Consumer<PubSubMessage> handler) {
        return delegate.patternSubscribe(pattern, handler).toFuture();
    }

    @Override
    public CompletableFuture<Void> patternUnsubscribe(String pattern) {
        return delegate.patternUnsubscribe(pattern).toFuture();
    }

    @Override
    public CompletableFuture<Void> publish(String channel, Object object) {
        return delegate.publish(channel, object).toFuture();
    }
}
