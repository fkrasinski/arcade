package com.arcade.redis;

import io.lettuce.core.pubsub.api.reactive.ChannelMessage;
import io.lettuce.core.pubsub.api.reactive.PatternMessage;

import java.util.Arrays;

public record PubSubMessage(
        String channel,
        byte[] content
) {

    public static PubSubMessage from(String channel, byte[] content) {
        return new PubSubMessage(channel, content);
    }

    public static PubSubMessage fromChannelMessage(ChannelMessage<String, byte[]> message) {
        return new PubSubMessage(message.getChannel(), message.getMessage());
    }

    public static PubSubMessage fromPatternMessage(PatternMessage<String, byte[]> message) {
        return new PubSubMessage(message.getChannel(), message.getMessage());
    }

    @Override
    public String toString() {
        return "PubSubMessage{" +
                "channel='" + channel + '\'' +
                ", content=[" + content.length + "bytes]" +
                '}';
    }
}
