package com.arcade.redis;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public interface RedisPubSubAsync {

    CompletableFuture<Void> subscribe(String channel, Consumer<PubSubMessage> handler);

    CompletableFuture<Void> unsubscribe(String channel);

    CompletableFuture<Void> patternSubscribe(String pattern, Consumer<PubSubMessage> handler);

    CompletableFuture<Void> patternUnsubscribe(String pattern);

    CompletableFuture<Void> publish(String channel, Object object);

}
