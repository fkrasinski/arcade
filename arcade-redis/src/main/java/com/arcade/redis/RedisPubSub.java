package com.arcade.redis;

import reactor.core.publisher.Mono;

import java.util.function.Consumer;

public interface RedisPubSub {

    Mono<Void> subscribe(String channel, Consumer<PubSubMessage> handler);

    Mono<Void> unsubscribe(String channel);

    Mono<Void> patternSubscribe(String pattern, Consumer<PubSubMessage> handler);

    Mono<Void> patternUnsubscribe(String pattern);

    Mono<Void> publish(String channel, Object object);

}
