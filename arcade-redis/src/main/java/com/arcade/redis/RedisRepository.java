package com.arcade.redis;

import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;

public interface RedisRepository {

    <T> Mono<T> get(String key, Class<T> clazz);

    <T> Mono<T> get(String key, String hashKey, Class<T> clazz);

    Mono<Void> set(String key, Object value);

    Mono<Void> set(String key, String hashKey, Object value);

    Mono<Void> set(String key, Object value, Duration duration);

    Mono<Boolean> hasKey(String key);

    Mono<Boolean> hasKey(String key, String hashKey);

    <T> Mono<Map<String, T>> getAllByKey(String key, Class<T> clazz);

    Mono<Void> delete(String key);

    Mono<Void> delete(String key, String hashKey);

}
