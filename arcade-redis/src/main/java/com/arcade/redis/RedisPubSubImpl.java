package com.arcade.redis;

import com.arcade.redis.converter.Converter;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.reactive.RedisPubSubReactiveCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Function;

public class RedisPubSubImpl implements RedisPubSub {
    private static final Logger logger = LoggerFactory.getLogger(RedisPubSubImpl.class);

    private final Converter converter;
    private final ConcurrentMap<String, Consumer<PubSubMessage>> channelHandlers;
    private final ConcurrentMap<String, Consumer<PubSubMessage>> patternHandlers;
    private final StatefulRedisPubSubConnection<String, byte[]> subscribeConnection;
    private final StatefulRedisPubSubConnection<String, byte[]> publishConnection;
    private final RedisPubSubReactiveCommands<String, byte[]> subscribeCommands;
    private final RedisPubSubReactiveCommands<String, byte[]> publishCommands;
    private final Object channelsLock;
    private final Object patternsLock;

    private Disposable channelsObserverSubscription;
    private Disposable patternsObserverSubscription;

    public RedisPubSubImpl(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        this.converter = converter;
        this.subscribeConnection = redisConnectionFactory.createPubSubConnection();
        this.publishConnection = redisConnectionFactory.createPubSubConnection();
        this.subscribeCommands = subscribeConnection.reactive();
        this.publishCommands = publishConnection.reactive();
        this.channelHandlers = new ConcurrentHashMap<>();
        this.patternHandlers = new ConcurrentHashMap<>();
        this.channelsLock = new Object();
        this.patternsLock = new Object();
        Runtime.getRuntime().addShutdownHook(new Thread(this::cleanUp)); //todo: IDK?
    }

    @Override
    public Mono<Void> subscribe(String channel, Consumer<PubSubMessage> handler) {
        return subscribeCommands.subscribe(channel)
                                .doFirst(() -> addChannelHandler(channel, handler));
    }

    @Override
    public Mono<Void> unsubscribe(String channel) {
        return subscribeCommands.unsubscribe(channel)
                                .doFirst(() -> delChannelHandler(channel));
    }

    @Override
    public Mono<Void> patternSubscribe(String pattern, Consumer<PubSubMessage> handler) {
        return subscribeCommands.psubscribe(pattern)
                                .doFirst(() -> addPatternHandler(pattern, handler));
    }

    @Override
    public Mono<Void> patternUnsubscribe(String pattern) {
        return subscribeCommands.punsubscribe(pattern)
                                .doFirst(() -> delPatternHandler(pattern));
    }

    @Override
    public Mono<Void> publish(String channel, Object object) {
        return Mono.just(converter.toBytes("_", object))
                   .doOnNext(__ -> logger.debug("Publishing on channel {} value {}", channel, object))
                   .flatMap(converted -> publishCommands.publish(channel, converted))
                   .then();
    }

    private void cleanUp() {
        logger.info("Shutting down redis pubsub connections");
        synchronized (channelsLock) {
            if (channelsObserverSubscription != null) {
                channelsObserverSubscription.dispose();
            }
        }
        synchronized (patternsLock) {
            if (patternsObserverSubscription != null) {
                patternsObserverSubscription.dispose();
            }
        }
        subscribeConnection.close();
        publishConnection.close();
    }

    private Disposable startListeningToChannels() {
        return subscribeCommands.observeChannels()
                                .map(PubSubMessage::fromChannelMessage)
                                .doOnNext(message -> channelHandlers.getOrDefault(message.channel(), Function.identity()::apply).accept(message))
                                .subscribe();
    }

    private Disposable startListeningToPatterns() {
        return subscribeCommands.observePatterns()
                                .map(PubSubMessage::fromPatternMessage)
                                .doOnNext(message -> patternHandlers.getOrDefault(message.channel(), Function.identity()::apply).accept(message))
                                .subscribe();
    }

    private void addChannelHandler(String channel, Consumer<PubSubMessage> handler) {
        synchronized (channelsLock) {
            channelHandlers.put(channel, handler);
            if (channelsObserverSubscription == null) {
                channelsObserverSubscription = startListeningToChannels();
            }
        }
    }

    private void addPatternHandler(String channel, Consumer<PubSubMessage> handler) {
        synchronized (patternsLock) {
            patternHandlers.put(channel, handler);
            if (patternsObserverSubscription == null) {
                channelsObserverSubscription = startListeningToPatterns();
            }
        }
    }

    private void delChannelHandler(String channel) {
        synchronized (channelsLock) {
            channelHandlers.remove(channel);
            if (channelHandlers.isEmpty()) {
                channelsObserverSubscription.dispose();
                channelsObserverSubscription = null;
            }
        }
    }

    private void delPatternHandler(String pattern) {
        synchronized (patternsLock) {
            patternHandlers.remove(pattern);
            if (channelHandlers.isEmpty()) {
                patternsObserverSubscription.dispose();
                patternsObserverSubscription = null;
            }
        }
    }
}
