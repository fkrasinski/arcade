package com.arcade.model;

public enum ServerType {
    PROXY,
    CHEST_PVP,
    LOBBY,
}
