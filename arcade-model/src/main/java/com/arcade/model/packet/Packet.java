package com.arcade.model.packet;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = HeartbeatPacket.class, name = PacketType.Constants.HEARTBEAT),
        @JsonSubTypes.Type(value = AckPacket.class, name = PacketType.Constants.ACK),
        @JsonSubTypes.Type(value = ServerRegisteredPacket.class, name = PacketType.Constants.SERVER_REGISTERED),
        @JsonSubTypes.Type(value = ServerRegisterRequestPacket.class, name = PacketType.Constants.SERVER_REGISTER_REQUEST),
        @JsonSubTypes.Type(value = ServerUnregisterPacket.class, name = PacketType.Constants.SERVER_UNREGISTER),
        @JsonSubTypes.Type(value = TransferPlayerPacket.class, name = PacketType.Constants.TRANSFER_PLAYER),
        @JsonSubTypes.Type(value = IdentifyProxyUserRequestPacket.class, name = PacketType.Constants.IDENTIFY_PROXY_USER_REQUEST),
        @JsonSubTypes.Type(value = IdentifyProxyUserResponsePacket.class, name = PacketType.Constants.IDENTIFY_PROXY_USER_RESPONSE),
        @JsonSubTypes.Type(value = UpdateServerStatisticsPacket.class, name = PacketType.Constants.UPDATE_SERVER_STATISTICS),
        @JsonSubTypes.Type(value = ServerSwitchPacket.class, name = PacketType.Constants.SERVER_SWITCH_PACKET),
})
public abstract class Packet implements Serializable {

    private final PacketType type;
    private long callbackId = ThreadLocalRandom.current().nextLong(Long.MIN_VALUE, Long.MAX_VALUE);

    public Packet(PacketType type) {
        this.type = type;
    }

    public PacketType getType() {
        return type;
    }

    public long getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(long callbackId) {
        this.callbackId = callbackId;
    }
}
