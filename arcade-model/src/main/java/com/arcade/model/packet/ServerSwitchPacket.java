package com.arcade.model.packet;

import java.util.UUID;

public class ServerSwitchPacket extends Packet {
    private UUID playerUniqueId;
    private String targetServerId;

    public ServerSwitchPacket() {
        super(PacketType.SERVER_SWITCH_PACKET);
    }

    public UUID getPlayerUniqueId() {
        return playerUniqueId;
    }

    public void setPlayerUniqueId(UUID playerUniqueId) {
        this.playerUniqueId = playerUniqueId;
    }

    public String getTargetServerId() {
        return targetServerId;
    }

    public void setTargetServerId(String targetServerId) {
        this.targetServerId = targetServerId;
    }
}
