package com.arcade.model.packet;

public enum PacketType {
    HEARTBEAT(Constants.HEARTBEAT),
    ACK(Constants.ACK),
    SERVER_REGISTERED(Constants.SERVER_REGISTERED),
    SERVER_REGISTER_REQUEST(Constants.SERVER_REGISTER_REQUEST),
    SERVER_UNREGISTER(Constants.SERVER_UNREGISTER),
    TRANSFER_PLAYER(Constants.TRANSFER_PLAYER),
    IDENTIFY_PROXY_USER_REQUEST(Constants.IDENTIFY_PROXY_USER_REQUEST),
    IDENTIFY_PROXY_USER_RESPONSE(Constants.IDENTIFY_PROXY_USER_RESPONSE),
    UPDATE_SERVER_STATISTICS(Constants.UPDATE_SERVER_STATISTICS),
    SERVER_SWITCH_PACKET(Constants.SERVER_SWITCH_PACKET);

    PacketType(String packetString) {
        if (!packetString.equals(this.name()))
            throw new IllegalArgumentException();
    }

    public static class Constants {
        public static final String HEARTBEAT = "HEARTBEAT";
        public static final String ACK = "ACK";
        public static final String SERVER_REGISTERED = "SERVER_REGISTERED";
        public static final String SERVER_REGISTER_REQUEST = "SERVER_REGISTER_REQUEST";
        public static final String SERVER_UNREGISTER = "SERVER_UNREGISTER";
        public static final String TRANSFER_PLAYER = "TRANSFER_PLAYER";
        public static final String IDENTIFY_PROXY_USER_REQUEST = "IDENTIFY_PROXY_USER_REQUEST";
        public static final String IDENTIFY_PROXY_USER_RESPONSE = "IDENTIFY_PROXY_USER_RESPONSE";
        public static final String UPDATE_SERVER_STATISTICS = "UPDATE_SERVER_STATISTICS";
        public static final String SERVER_SWITCH_PACKET = "SERVER_SWITCH_PACKET";
    }
}
