package com.arcade.model.packet;

import java.util.UUID;

public class IdentifyProxyUserResponsePacket extends Packet {
    private UUID userUniqueId;
    private String proxyUniqueId;

    public IdentifyProxyUserResponsePacket() {
        super(PacketType.IDENTIFY_PROXY_USER_RESPONSE);
    }

    public UUID getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(UUID userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getProxyUniqueId() {
        return proxyUniqueId;
    }

    public void setProxyUniqueId(String proxyUniqueId) {
        this.proxyUniqueId = proxyUniqueId;
    }
}
