package com.arcade.model.packet;

public class ServerUnregisterPacket extends Packet {
    private String id;

    public ServerUnregisterPacket() {
        super(PacketType.SERVER_UNREGISTER);
    }

    public ServerUnregisterPacket(String id) {
        super(PacketType.SERVER_UNREGISTER);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
