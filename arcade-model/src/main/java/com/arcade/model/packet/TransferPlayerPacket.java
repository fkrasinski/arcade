package com.arcade.model.packet;

import java.util.UUID;

public class TransferPlayerPacket extends Packet {

    private String serverTarget;
    private String requestingServer;
    private UUID playerUniqueId;
    private String nickname;
    private String gameMode;
    private byte[] inventory;
    private byte[] armor;
    private byte[] enderChest;
    private byte[] potionEffects;
    private byte[] location;
    private double health;
    private float walkSpeed;
    private float flySpeed;
    private float experience;
    private int totalExperience;
    private int level;
    private int foodLevel;
    private int fireTicks;
    private int remainingAir;
    private int heldSlot;
    private boolean allowFlight;
    private boolean flying;
    private boolean op;
    private boolean sprinting;
    private boolean sneaking;

    public TransferPlayerPacket() {
        super(PacketType.TRANSFER_PLAYER);
    }

    public String getServerTarget() {
        return serverTarget;
    }

    public void setServerTarget(String serverTarget) {
        this.serverTarget = serverTarget;
    }

    public String getRequestingServer() {
        return requestingServer;
    }

    public void setRequestingServer(String requestingServer) {
        this.requestingServer = requestingServer;
    }

    public UUID getPlayerUniqueId() {
        return playerUniqueId;
    }

    public void setPlayerUniqueId(UUID playerUniqueId) {
        this.playerUniqueId = playerUniqueId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public byte[] getInventory() {
        return inventory;
    }

    public void setInventory(byte[] inventory) {
        this.inventory = inventory;
    }

    public byte[] getArmor() {
        return armor;
    }

    public void setArmor(byte[] armor) {
        this.armor = armor;
    }

    public byte[] getEnderChest() {
        return enderChest;
    }

    public void setEnderChest(byte[] enderChest) {
        this.enderChest = enderChest;
    }

    public byte[] getPotionEffects() {
        return potionEffects;
    }

    public void setPotionEffects(byte[] potionEffects) {
        this.potionEffects = potionEffects;
    }

    public byte[] getLocation() {
        return location;
    }

    public void setLocation(byte[] location) {
        this.location = location;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public float getWalkSpeed() {
        return walkSpeed;
    }

    public void setWalkSpeed(float walkSpeed) {
        this.walkSpeed = walkSpeed;
    }

    public float getFlySpeed() {
        return flySpeed;
    }

    public void setFlySpeed(float flySpeed) {
        this.flySpeed = flySpeed;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(float experience) {
        this.experience = experience;
    }

    public int getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(int totalExperience) {
        this.totalExperience = totalExperience;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getFoodLevel() {
        return foodLevel;
    }

    public void setFoodLevel(int foodLevel) {
        this.foodLevel = foodLevel;
    }

    public int getFireTicks() {
        return fireTicks;
    }

    public void setFireTicks(int fireTicks) {
        this.fireTicks = fireTicks;
    }

    public int getRemainingAir() {
        return remainingAir;
    }

    public void setRemainingAir(int remainingAir) {
        this.remainingAir = remainingAir;
    }

    public int getHeldSlot() {
        return heldSlot;
    }

    public void setHeldSlot(int heldSlot) {
        this.heldSlot = heldSlot;
    }

    public boolean isAllowFlight() {
        return allowFlight;
    }

    public void setAllowFlight(boolean allowFlight) {
        this.allowFlight = allowFlight;
    }

    public boolean isFlying() {
        return flying;
    }

    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public boolean isOp() {
        return op;
    }

    public void setOp(boolean op) {
        this.op = op;
    }

    public boolean isSprinting() {
        return sprinting;
    }

    public void setSprinting(boolean sprinting) {
        this.sprinting = sprinting;
    }

    public boolean isSneaking() {
        return sneaking;
    }

    public void setSneaking(boolean sneaking) {
        this.sneaking = sneaking;
    }
}
