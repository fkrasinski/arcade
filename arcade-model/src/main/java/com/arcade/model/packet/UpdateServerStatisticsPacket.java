package com.arcade.model.packet;

public class UpdateServerStatisticsPacket extends Packet {
    private String serverId;
    private int playersCount;
    private double tps;

    public UpdateServerStatisticsPacket() {
        super(PacketType.UPDATE_SERVER_STATISTICS);
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public int getPlayersCount() {
        return playersCount;
    }

    public void setPlayersCount(int playersCount) {
        this.playersCount = playersCount;
    }

    public double getTps() {
        return tps;
    }

    public void setTps(double tps) {
        this.tps = tps;
    }
}
