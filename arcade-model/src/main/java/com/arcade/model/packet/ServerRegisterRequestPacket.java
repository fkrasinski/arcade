package com.arcade.model.packet;

import com.arcade.model.ServerInfo;

public class ServerRegisterRequestPacket extends Packet {
    private ServerInfo serverInfo;

    public ServerRegisterRequestPacket() {
        super(PacketType.SERVER_REGISTER_REQUEST);
    }

    public ServerRegisterRequestPacket(ServerInfo serverInfo) {
        super(PacketType.SERVER_REGISTER_REQUEST);
        this.serverInfo = serverInfo;
    }

    public ServerInfo getServerInfo() {
        return serverInfo;
    }

    public void setServerInfo(ServerInfo serverInfo) {
        this.serverInfo = serverInfo;
    }
}
