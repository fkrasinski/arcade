package com.arcade.model.packet;

import java.util.UUID;

public class IdentifyProxyUserRequestPacket extends Packet {
    private UUID userUniqueId;
    private String requestingServerUniqueId;

    public IdentifyProxyUserRequestPacket() {
        super(PacketType.IDENTIFY_PROXY_USER_REQUEST);
    }

    public UUID getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(UUID userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getRequestingServerUniqueId() {
        return requestingServerUniqueId;
    }

    public void setRequestingServerUniqueId(String requestingServerUniqueId) {
        this.requestingServerUniqueId = requestingServerUniqueId;
    }
}
