package com.arcade.model.packet;

public class HeartbeatPacket extends Packet {
    private String id;
    private long millis;

    public HeartbeatPacket() {
        super(PacketType.HEARTBEAT);
    }

    public HeartbeatPacket(String id, long millis) {
        super(PacketType.HEARTBEAT);
        this.id = id;
        this.millis = millis;
    }

    public String getId() {
        return id;
    }

    public long getMillis() {
        return millis;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
