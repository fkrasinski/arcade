package com.arcade.model.packet;

public class AckPacket extends Packet {

    public AckPacket() {
        super(PacketType.ACK);
    }
}
