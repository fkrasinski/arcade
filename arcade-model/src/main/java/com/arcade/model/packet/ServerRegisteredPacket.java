package com.arcade.model.packet;

import com.arcade.model.ServerInfo;

public class ServerRegisteredPacket extends Packet {
    private ServerInfo serverInfo;

    public ServerRegisteredPacket() {
        super(PacketType.SERVER_REGISTERED);
    }

    public ServerRegisteredPacket(ServerInfo serverInfo) {
        super(PacketType.SERVER_REGISTERED);
        this.serverInfo = serverInfo;
    }

    public ServerInfo getServerInfo() {
        return serverInfo;
    }

    public void setServerInfo(ServerInfo serverInfo) {
        this.serverInfo = serverInfo;
    }

    @Override
    public String toString() {
        return "ServerRegisteredPacket{" +
                "serverInfo=" + serverInfo +
                '}';
    }
}
