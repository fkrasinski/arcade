package com.arcade.model;

public class Channels {

    public static final String GLOBAL  = "ARCADE_GLOBAL";
    public static final String MASTER  = "ARCADE_MASTER";
    public static final String PROXIES = "ARCADE_PROXIES";

    public static String server(String id) {
        return String.format("ARCADE_SERVER_%s", id);
    }

    public static String proxy(String id) {
        return String.format("ARCADE_PROXY_%s", id);
    }
}
