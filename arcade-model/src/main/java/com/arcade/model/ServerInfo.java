package com.arcade.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record ServerInfo(
        String id,
        ServerType serverType,
        String host,
        int port,
        int playersCount,
        double tps,
        long lastHeartbeat) {

    @JsonCreator
    public static ServerInfo create(@JsonProperty("id") String id,
                                    @JsonProperty("serverType") ServerType serverType,
                                    @JsonProperty("host") String host,
                                    @JsonProperty("port") int port,
                                    @JsonProperty("playersCount") int playersCount,
                                    @JsonProperty("tps") double tps,
                                    @JsonProperty("lastHeartbeat") long lastHeartbeat) {
        return new ServerInfo(id, serverType, host, port, playersCount, tps, lastHeartbeat);
    }

    @Override
    public String toString() {
        return "ServerInfo{" +
                "id='" + id + '\'' +
                ", serverType=" + serverType +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", playersCount=" + playersCount +
                ", tps=" + tps +
                ", lastHeartbeat=" + lastHeartbeat +
                '}';
    }
}
