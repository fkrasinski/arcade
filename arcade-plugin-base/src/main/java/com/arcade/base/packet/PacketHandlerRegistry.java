package com.arcade.base.packet;

import com.arcade.model.packet.Packet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PacketHandlerRegistry {

    private final Map<Class<? extends Packet>, List<PacketHandler<? extends Packet>>> handlers;

    public PacketHandlerRegistry() {
        this.handlers = new ConcurrentHashMap<>();
    }

    public <T extends Packet> void addHandler(Class<T> clazz, PacketHandler<T> handler) {
        var packetHandlers = handlers.computeIfAbsent(clazz, __ -> new ArrayList<>());
        packetHandlers.add(handler);
    }

    public <T extends Packet> List<PacketHandler<? extends Packet>> getHandlers(Class<T> clazz) {
        return handlers.computeIfAbsent(clazz, key -> new ArrayList<>());
    }
}
