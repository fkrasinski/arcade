package com.arcade.base.packet;

import com.arcade.model.packet.Packet;

import java.util.concurrent.CompletionStage;

public interface RedisPubSubPacket {

    CompletionStage<Void> subscribe(String channel);

    CompletionStage<Void> unsubscribe(String channel);

    CompletionStage<Void> publish(String channel, Packet packet);

    <T extends Packet> CompletionStage<Void> publishWithCallback(String channel, Packet packet, Callback<T> callback);
}
