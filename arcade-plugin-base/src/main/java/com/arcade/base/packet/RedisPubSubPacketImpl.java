package com.arcade.base.packet;

import com.arcade.model.packet.Packet;
import com.arcade.redis.RedisPubSubAsync;
import com.arcade.redis.converter.Converter;
import com.google.inject.Inject;

import java.util.concurrent.CompletionStage;

public class RedisPubSubPacketImpl implements RedisPubSubPacket {

    private final RedisPubSubAsync delegate;
    private final RedisPubSubMessageHandler handler;
    private final CallbackCache callbackCache;
    private final Converter converter;

    @Inject
    public RedisPubSubPacketImpl(RedisPubSubAsync delegate,
                                 RedisPubSubMessageHandler handler,
                                 CallbackCache callbackCache,
                                 Converter converter) {
        this.delegate = delegate;
        this.handler = handler;
        this.callbackCache = callbackCache;
        this.converter = converter;
    }

    @Override
    public CompletionStage<Void> subscribe(String channel) {
        return delegate.subscribe(channel, handler);
    }

    @Override
    public CompletionStage<Void> unsubscribe(String channel) {
        return delegate.unsubscribe(channel);
    }

    @Override
    public CompletionStage<Void> publish(String channel, Packet packet) {
        return delegate.publish(channel, packet);
    }

    @Override
    public <T extends Packet> CompletionStage<Void> publishWithCallback(String channel, Packet packet, Callback<T> callback) {
        callbackCache.push(packet.getCallbackId(), callback);
        return delegate.publish(channel, packet);
    }
}
