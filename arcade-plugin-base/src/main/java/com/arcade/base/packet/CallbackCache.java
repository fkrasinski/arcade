package com.arcade.base.packet;

import com.arcade.model.packet.Packet;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CallbackCache {

    private final Map<Long, Callback<? extends Packet>> callbacks;

    public CallbackCache() {
        this.callbacks = new ConcurrentHashMap<>();
    }

    public Callback<? extends Packet> pull(long id) {
        return callbacks.remove(id);
    }

    public void push(long id, Callback<?> callback) {
        callbacks.put(id, callback);
    }
}
