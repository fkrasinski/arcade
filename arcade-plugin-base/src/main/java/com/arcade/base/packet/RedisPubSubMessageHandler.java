package com.arcade.base.packet;

import com.arcade.model.packet.Packet;
import com.arcade.redis.PubSubMessage;
import com.arcade.redis.converter.Converter;
import com.arcade.redis.exception.ConverterException;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.function.Consumer;

public class RedisPubSubMessageHandler implements Consumer<PubSubMessage> {
    private static final Logger logger = LoggerFactory.getLogger(RedisPubSubMessageHandler.class);

    private final PacketHandlerRegistry packetHandlerRegistry;
    private final CallbackCache callbackCache;
    private final Converter converter;

    @Inject
    public RedisPubSubMessageHandler(PacketHandlerRegistry packetHandlerRegistry,
                                     CallbackCache callbackCache,
                                     Converter converter) {
        this.packetHandlerRegistry = packetHandlerRegistry;
        this.callbackCache = callbackCache;
        this.converter = converter;
    }

    @Override
    public void accept(PubSubMessage message) {
        try {
            logger.info("Handling pubsub message: {}", message);
            Packet packet = converter.fromBytes("_", message.content(), Packet.class);
            Callback<? extends Packet> callback = callbackCache.pull(packet.getCallbackId());
            if (Objects.nonNull(callback)) {
                handle(callback, packet);
                return;
            }

            var handlers = packetHandlerRegistry.getHandlers(packet.getClass());
            if (handlers.isEmpty()) {
                logger.info("Handler not found");
                return;
            }
            for (var handler : handlers) {
                handle(handler, packet);
            }
        } catch (ConverterException e) {
            logger.error("Failed to convert", e);
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private <T extends Packet> void handle(Callback<T> callback, Packet packet) {
        callback.accept((T) packet);
    }

    private <T extends Packet> void handle(PacketHandler<T> handler, Packet packet) {
        handler.handle((T) packet);
    }
}
