package com.arcade.base.packet;

import com.arcade.model.packet.Packet;

public interface PacketHandler<T extends Packet> {

    void handle(T packet);

}
