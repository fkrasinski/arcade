package com.arcade.base;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryRepository<K, V> {

    private final Map<K, V> data;

    public InMemoryRepository() {
        this.data = new ConcurrentHashMap<>();
    }

    public V get(K k) {
        return data.get(k);
    }

    public void put(K k, V v) {
        data.put(k, v);
    }

    public void remove(K k) {
        data.remove(k);
    }

    public boolean has(K k) {
        return data.containsKey(k);
    }
}
