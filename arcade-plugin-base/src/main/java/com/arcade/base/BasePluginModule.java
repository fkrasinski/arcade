package com.arcade.base;

import com.arcade.base.packet.CallbackCache;
import com.arcade.base.packet.PacketHandlerRegistry;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.base.packet.RedisPubSubPacketImpl;
import com.arcade.redis.*;
import com.arcade.redis.converter.Converter;
import com.arcade.redis.converter.ConverterImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.time.Duration;

public class BasePluginModule extends AbstractModule {

    @Override
    protected void configure() {
        binder().bind(CallbackCache.class).in(Singleton.class);
        binder().bind(PacketHandlerRegistry.class).in(Singleton.class);

//        bind(Converter.class).toInstance(new ConverterImpl(true));


//        bind(RedisPubSub.class).to(RedisPubSubImpl.class).in(Singleton.class);
//        bind(RedisPubSubAsync.class).to(RedisPubSubAsyncImpl.class).in(Singleton.class);
        bind(RedisPubSubPacket.class).to(RedisPubSubPacketImpl.class).in(Singleton.class);
//
//        bind(RedisRepository.class).to(RedisRepositoryImpl.class).in(Singleton.class);
//        bind(RedisRepositoryAsync.class).to(RedisRepositoryAsyncImpl.class).in(Singleton.class);
    }

    @Provides
    public Converter converter() {
        return new ConverterImpl(true);
    }

    @Provides
    public RedisConfig redisConfig(@Named("redis.config.nodeURI") String nodeURI,
                                   @Named("redis.config.token") String password,
                                   @Named("redis.config.timeout") Duration timeout) {
        return new RedisConfig(nodeURI, password, timeout);
    }

    @Provides
    public RedisConnectionFactory redisConnectionFactory(RedisConfig redisConfig) {
        return new RedisConnectionFactory(redisConfig);
    }

    @Provides
    public RedisRepository redisRepository(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        return new RedisRepositoryImpl(redisConnectionFactory, converter);
    }

    @Provides
    public RedisRepositoryAsync redisRepositoryAsync(RedisRepository redisRepository) {
        return new RedisRepositoryAsyncImpl(redisRepository);
    }

    @Provides
    public RedisPubSub redisPubSub(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        return new RedisPubSubImpl(redisConnectionFactory, converter);
    }

    @Provides
    public RedisPubSubAsync redisPubSubAsync(RedisPubSub redisPubSub) {
        return new RedisPubSubAsyncImpl(redisPubSub);
    }
}
