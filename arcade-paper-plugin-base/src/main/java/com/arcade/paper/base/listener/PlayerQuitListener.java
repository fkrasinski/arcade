package com.arcade.paper.base.listener;

import com.arcade.paper.base.ArcadePlugin;
import com.google.inject.Inject;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    private final ArcadePlugin arcadePlugin;

    @Inject
    public PlayerQuitListener(ArcadePlugin arcadePlugin) {
        this.arcadePlugin = arcadePlugin;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        this.arcadePlugin.proxyUsersStorage().remove(event.getPlayer().getUniqueId());
    }
}
