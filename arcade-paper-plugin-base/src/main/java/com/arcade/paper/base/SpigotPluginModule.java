package com.arcade.paper.base;

import com.arcade.base.BasePluginModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import java.time.Duration;

public class SpigotPluginModule extends AbstractModule {

    private final ArcadePlugin javaPlugin;

    public SpigotPluginModule(ArcadePlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    @Override
    protected void configure() {
        install(new BasePluginModule());

        bindConstant().annotatedWith(Names.named("redis.config.nodeURI")).to("redis://127.0.0.1:6379");
        bindConstant().annotatedWith(Names.named("redis.config.token")).to("");
    }

    @Provides
    @Named("redis.config.timeout")
    public Duration provideTimeout() {
        return Duration.ofSeconds(10);
    }

    @Provides
    public ArcadePlugin plugin() {
        return javaPlugin;
    }
}
