package com.arcade.paper.base.model;

import java.util.UUID;

public class ProxyUser {
    private final UUID uniqueId;
    private final String proxyId;

    public ProxyUser(UUID uniqueId, String proxyId) {
        this.uniqueId = uniqueId;
        this.proxyId = proxyId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getProxyId() {
        return proxyId;
    }
}
