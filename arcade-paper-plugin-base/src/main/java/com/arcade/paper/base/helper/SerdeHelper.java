package com.arcade.paper.base.helper;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SerdeHelper {

    public static byte[] serializeItems(ItemStack[] items) throws IOException {
        try (var stream = new ByteArrayOutputStream()) {

            stream.write(items.length);
            for (ItemStack item : items) {
                if (item == null) {
                    stream.write(0);
                } else {
                    byte[] itemBytes = item.serializeAsBytes();
                    stream.write(itemBytes.length);
                    stream.write(itemBytes);
                }
            }

            return stream.toByteArray();
        }
    }

    public static ItemStack[] deserializeItems(byte[] bytes) throws IOException {
        try (var stream = new ByteArrayInputStream(bytes)) {

            ItemStack[] items = new ItemStack[stream.read()];

            for (int i = 0; i < items.length; i++) {
                int size = stream.read();
                if (size == 0) {
                    items[i] = null;
                } else {
                    byte[] itemBytes = stream.readNBytes(size);
                    items[i] = ItemStack.deserializeBytes(itemBytes);
                }
            }

            return items;
        }
    }

    public static byte[] serializeLocation(Location location) throws IOException {
        try (var baos = new ByteArrayOutputStream();
             var stream = new DataOutputStream(baos)) {

            stream.writeDouble(location.getX());
            stream.writeDouble(location.getY());
            stream.writeDouble(location.getZ());
            stream.writeFloat(location.getYaw());
            stream.writeFloat(location.getPitch());

            return baos.toByteArray();
        }
    }

    public static Location deserializeLocation(byte[] bytes) throws IOException {
        try (var bais = new ByteArrayInputStream(bytes);
             var stream = new DataInputStream(bais)) {

            var x = stream.readDouble();
            var y = stream.readDouble();
            var z = stream.readDouble();
            var yaw = stream.readFloat();
            var pitch = stream.readFloat();

            return new Location(null, x, y, z, yaw, pitch);
        }
    }

    public static byte[] serializePotionEffects(Collection<PotionEffect> effects) throws IOException {
        try (var baos = new ByteArrayOutputStream();
             var stream = new DataOutputStream(baos)) {

            stream.writeInt(effects.size());
            for (PotionEffect effect : effects) {
                stream.writeUTF(effect.getType().getKey().toString());
                stream.writeInt(effect.getDuration());
                stream.writeInt(effect.getAmplifier());
                stream.writeBoolean(effect.isAmbient());
                stream.writeBoolean(effect.hasParticles());
                stream.writeBoolean(effect.hasIcon());
            }

            return baos.toByteArray();
        }
    }

    public static Collection<PotionEffect> deserializePotionEffects(byte[] bytes) throws IOException {
        try (var bais = new ByteArrayInputStream(bytes);
             var stream = new DataInputStream(bais)) {

            int effectsCount = stream.readInt();
            List<PotionEffect> effects = new ArrayList<>(effectsCount);

            for (int i = 0; i < effectsCount; i++) {
                String type = stream.readUTF();
                int duration = stream.readInt();
                int amplifier = stream.readInt();
                var isAmbient = stream.readBoolean();
                var hasParticles = stream.readBoolean();
                var hasIcon = stream.readBoolean();

                effects.add(new PotionEffect(
                        PotionEffectType.getByKey(NamespacedKey.fromString(type)),
                        duration,
                        amplifier,
                        isAmbient,
                        hasParticles,
                        hasIcon
                ));
            }

            return effects;
        }
    }
}
