package com.arcade.paper.base;

import com.arcade.base.InMemoryRepository;
import com.arcade.base.packet.Callback;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.model.Channels;
import com.arcade.model.ServerInfo;
import com.arcade.model.ServerType;
import com.arcade.model.packet.*;
import com.arcade.paper.base.listener.AsyncPlayerPreLoginListener;
import com.arcade.paper.base.listener.PlayerQuitListener;
import com.arcade.paper.base.model.ProxyUser;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main extends JavaPlugin implements ArcadePlugin {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private final Injector injector;
    private final String serverId;
    private final InMemoryRepository<UUID, ProxyUser> proxyUsers;

    public Main() {
        this.serverId = UUID.randomUUID().toString();
        this.injector = Guice.createInjector(new SpigotPluginModule(this));
        this.proxyUsers = new InMemoryRepository<>();
    }

    @Override
    public void onEnable() {
        logger.info("Enabling server (id: {})", getServerId());
        registerListeners();
        tellMaster();
        heartbeat();
        statistics();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public Injector getBaseInjector() {
        return injector;
    }

    @Override
    public String getServerId() {
        return serverId;
    }

    @Override
    public InMemoryRepository<UUID, ProxyUser> proxyUsersStorage() {
        return proxyUsers;
    }

    private void registerListeners() {
        var pm = getServer().getPluginManager();
        pm.registerEvents(injector.getInstance(AsyncPlayerPreLoginListener.class), this);
        pm.registerEvents(injector.getInstance(PlayerQuitListener.class), this);
    }

    private void tellMaster() {
        RedisPubSubPacket pubSub = injector.getInstance(RedisPubSubPacket.class);
        CompletableFuture<Void> latch = new CompletableFuture<>();

        // specific server channel
        pubSub.subscribe(Channels.server(getServerId()));

        logger.info("Telling master about new server");
        pubSub.publishWithCallback(Channels.MASTER, serverRegitrationPacket(),
                (Callback<ServerRegisteredPacket>) response -> {
                    logger.info("Got confirmation from master");
                    latch.complete(null);
                });

        try {
            logger.info("Waiting for confirmation from master");
            latch.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("Didn't receive confirmation from master in 10 seconds", e);
            Bukkit.getServer().shutdown();
        }
    }

    private void heartbeat() {
        RedisPubSubPacket pubSub = injector.getInstance(RedisPubSubPacket.class);
        // todo: probably shouldn't be bukkit scheduler
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
            Packet packet = new HeartbeatPacket(getServerId(), System.currentTimeMillis());
            pubSub.publish(Channels.MASTER, packet);
        }, 20, 20);
    }

    // do it with heartbeat?
    private void statistics() {
        RedisPubSubPacket pubSub = injector.getInstance(RedisPubSubPacket.class);
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
            UpdateServerStatisticsPacket packet = new UpdateServerStatisticsPacket();
            packet.setServerId(getServerId());
            packet.setPlayersCount(Bukkit.getOnlinePlayers().size());
            packet.setTps(Bukkit.getTPS()[0]);
            pubSub.publish(Channels.MASTER, packet);
        }, 20, 20);
    }

    private Packet serverRegitrationPacket() {
        return new ServerRegisterRequestPacket(new ServerInfo(
                getServerId(),
                ServerType.CHEST_PVP,
                "[::1]",
                getServer().getPort(),
                Bukkit.getOnlinePlayers().size(),
                Bukkit.getTPS()[0],
                System.currentTimeMillis()));
    }
}
