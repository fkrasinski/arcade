package com.arcade.paper.base;

import com.arcade.base.InMemoryRepository;
import com.arcade.paper.base.model.ProxyUser;
import com.google.inject.Injector;

import java.util.UUID;

public interface ArcadePlugin {

    Injector getBaseInjector();

    String getServerId();

    InMemoryRepository<UUID, ProxyUser> proxyUsersStorage();

}
