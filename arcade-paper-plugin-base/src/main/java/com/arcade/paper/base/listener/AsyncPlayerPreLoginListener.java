package com.arcade.paper.base.listener;

import com.arcade.base.packet.Callback;
import com.arcade.base.packet.RedisPubSubPacket;
import com.arcade.model.Channels;
import com.arcade.model.packet.IdentifyProxyUserRequestPacket;
import com.arcade.model.packet.IdentifyProxyUserResponsePacket;
import com.arcade.paper.base.ArcadePlugin;
import com.arcade.paper.base.model.ProxyUser;
import com.google.inject.Inject;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AsyncPlayerPreLoginListener implements Listener {
    private static final Logger logger = LoggerFactory.getLogger(AsyncPlayerPreLoginListener.class);

    private final ArcadePlugin arcadePlugin;
    private final RedisPubSubPacket redisPubSub;

    @Inject
    public AsyncPlayerPreLoginListener(ArcadePlugin arcadePlugin, RedisPubSubPacket redisPubSub) {
        this.arcadePlugin = arcadePlugin;
        this.redisPubSub = redisPubSub;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPreLogin(AsyncPlayerPreLoginEvent event) {
        UUID userUniqueId = event.getUniqueId();
        CompletableFuture<Void> latch = new CompletableFuture<>();
        redisPubSub.publishWithCallback(Channels.PROXIES, identifyProxyUserRequestPacket(userUniqueId), (Callback<IdentifyProxyUserResponsePacket>) response -> {
            logger.info("Identified users proxy (user uuid: {}) (proxy uuid: {})", userUniqueId, response.getProxyUniqueId());
            arcadePlugin.proxyUsersStorage().put(userUniqueId, new ProxyUser(userUniqueId, response.getProxyUniqueId()));
            latch.complete(null);
        });

        try {
            latch.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("Didn't receive response of type IdentifyProxyUserResponsePacket after 10 seconds.");
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, Component.text("Error occurred", NamedTextColor.RED));
        }
    }

    private IdentifyProxyUserRequestPacket identifyProxyUserRequestPacket(UUID userUniqueId) {
        var packet = new IdentifyProxyUserRequestPacket();
        packet.setUserUniqueId(userUniqueId);
        packet.setRequestingServerUniqueId(arcadePlugin.getServerId());
        return packet;
    }
}
