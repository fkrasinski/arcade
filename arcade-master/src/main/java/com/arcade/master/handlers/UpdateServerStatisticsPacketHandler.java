package com.arcade.master.handlers;

import com.arcade.model.ServerInfo;
import com.arcade.model.packet.PacketType;
import com.arcade.model.packet.UpdateServerStatisticsPacket;
import com.arcade.redis.RedisKeys;
import com.arcade.redis.RedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class UpdateServerStatisticsPacketHandler implements PacketHandler<UpdateServerStatisticsPacket> {
    private static final Logger logger = LoggerFactory.getLogger(HeartbeatPacketHandler.class);

    private final RedisRepository redisRepository;

    public UpdateServerStatisticsPacketHandler(RedisRepository redisRepository) {
        this.redisRepository = redisRepository;
    }

    @Override
    public void handle(UpdateServerStatisticsPacket packet) {
        redisRepository.get(RedisKeys.serversKey(), packet.getServerId(), ServerInfo.class)
                       .switchIfEmpty(Mono.defer(() -> Mono.fromRunnable(() -> logger.warn("Received UpdateServerStatisticsPacket packet from {} but server is not registered", packet.getServerId()))))
                       .flatMap(info -> redisRepository.set(RedisKeys.serversKey(), packet.getServerId(), update(info, packet)))
                       .subscribe();
    }

    @Override
    public PacketType forType() {
        return PacketType.UPDATE_SERVER_STATISTICS;
    }

    private ServerInfo update(ServerInfo info, UpdateServerStatisticsPacket packet) {
        return new ServerInfo(info.id(), info.serverType(), info.host(), info.port(), packet.getPlayersCount(), packet.getTps(), info.lastHeartbeat());
    }
}
