package com.arcade.master.handlers;

import com.arcade.model.packet.Packet;
import com.arcade.model.packet.PacketType;

public interface PacketHandler<T extends Packet> {

    PacketType forType();

    void handle(T packet);

}
