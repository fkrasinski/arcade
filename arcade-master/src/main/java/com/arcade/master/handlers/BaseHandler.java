package com.arcade.master.handlers;

import com.arcade.model.Channels;
import com.arcade.model.packet.Packet;
import com.arcade.model.packet.PacketType;
import com.arcade.redis.PubSubMessage;
import com.arcade.redis.RedisPubSub;
import com.arcade.redis.converter.Converter;
import com.arcade.redis.exception.ConverterException;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class BaseHandler implements Consumer<PubSubMessage> {
    private static final Logger logger = LoggerFactory.getLogger(BaseHandler.class);

    private final Converter converter;
    private final Map<PacketType, PacketHandler<? extends Packet>> handlers;
    private final Disposable disposable;

    public BaseHandler(Converter converter, List<PacketHandler<?>> handlers, RedisPubSub redisPubSub) {
        this.converter = converter;
        this.handlers = handlers.stream().collect(Collectors.toMap(PacketHandler::forType, Function.identity()));
        this.disposable = redisPubSub.subscribe(Channels.MASTER, this)
                                     .subscribe();
    }

    @Override
    public void accept(PubSubMessage message) {
        try {
            Packet packet = converter.fromBytes("_", message.content(), Packet.class);

            PacketHandler<? extends Packet> handler = handlers.get(packet.getType());
            if (Objects.isNull(handler)) {
                logger.warn("Handler not found for type {}", packet.getType());
                return;
            }

            handle(handler, packet);
        } catch (ConverterException e) {
            throw new RuntimeException(e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        disposable.dispose();
    }

    private <T extends Packet> void handle(PacketHandler<T> handler, Packet packet) {
        handler.handle((T) packet);
    }

}
