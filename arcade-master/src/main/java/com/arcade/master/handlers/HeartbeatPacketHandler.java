package com.arcade.master.handlers;

import com.arcade.model.ServerInfo;
import com.arcade.model.packet.HeartbeatPacket;
import com.arcade.model.packet.PacketType;
import com.arcade.redis.RedisKeys;
import com.arcade.redis.RedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class HeartbeatPacketHandler implements PacketHandler<HeartbeatPacket> {
    private static final Logger logger = LoggerFactory.getLogger(HeartbeatPacketHandler.class);

    private final RedisRepository redisRepository;

    public HeartbeatPacketHandler(RedisRepository redisRepository) {
        this.redisRepository = redisRepository;
    }

    @Override
    public void handle(HeartbeatPacket packet) {
        redisRepository.get(RedisKeys.serversKey(), packet.getId(), ServerInfo.class)
                       .switchIfEmpty(Mono.defer(() -> Mono.fromRunnable(() -> logger.warn("Received KeepAlivePacket packet from {} but server is not registered", packet.getId()))))
                       .flatMap(info -> redisRepository.set(RedisKeys.serversKey(), packet.getId(), update(info)))
                       .subscribe();
    }

    @Override
    public PacketType forType() {
        return PacketType.HEARTBEAT;
    }

    private ServerInfo update(ServerInfo info) {
        return new ServerInfo(info.id(), info.serverType(), info.host(), info.port(), info.playersCount(), info.tps(), System.currentTimeMillis());
    }
}
