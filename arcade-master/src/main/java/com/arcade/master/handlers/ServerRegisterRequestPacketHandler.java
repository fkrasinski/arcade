package com.arcade.master.handlers;

import com.arcade.model.Channels;
import com.arcade.model.ServerInfo;
import com.arcade.model.ServerType;
import com.arcade.model.packet.Packet;
import com.arcade.model.packet.PacketType;
import com.arcade.model.packet.ServerRegisterRequestPacket;
import com.arcade.model.packet.ServerRegisteredPacket;
import com.arcade.redis.RedisKeys;
import com.arcade.redis.RedisPubSub;
import com.arcade.redis.RedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class ServerRegisterRequestPacketHandler implements PacketHandler<ServerRegisterRequestPacket> {
    private static final Logger logger = LoggerFactory.getLogger(ServerRegisterRequestPacketHandler.class);

    private final RedisRepository redisRepository;
    private final RedisPubSub redisPubSub;

    public ServerRegisterRequestPacketHandler(RedisRepository redisRepository,
                                              RedisPubSub redisPubSub) {
        this.redisRepository = redisRepository;
        this.redisPubSub = redisPubSub;
    }

    @Override
    public void handle(ServerRegisterRequestPacket packet) {
        ServerInfo info = packet.getServerInfo();
        redisRepository.hasKey(RedisKeys.serversKey(), info.id())
                       .filter(result -> result)
                       .flatMap(__ -> getServerInfo(info.id()))
                       .switchIfEmpty(Mono.defer(() -> saveServer(packet)))
                       .flatMap(i -> announce(i, packet.getCallbackId()))
                       .doOnError(ex -> logger.error("Error registering server", ex))
                       .subscribe();
    }

    @Override
    public PacketType forType() {
        return PacketType.SERVER_REGISTER_REQUEST;
    }

    private Mono<ServerInfo> saveServer(ServerRegisterRequestPacket packet) {
        return Mono.just(createInfo(packet))
                   .flatMap(info -> redisRepository.set(RedisKeys.serversKey(), info.id(), info)
                                                   .thenReturn(info));
    }

    private Mono<Void> announce(ServerInfo serverInfo, long callbackId) {
        Packet packet = new ServerRegisteredPacket(serverInfo);

        Packet serverPacket = new ServerRegisteredPacket(serverInfo);
        serverPacket.setCallbackId(callbackId);

        var channel = serverInfo.serverType() == ServerType.PROXY
                ? Channels.proxy(serverInfo.id()) //todo: not ideal - no reason to tell proxies another proxy is up, separate packet?
                : Channels.server(serverInfo.id());
        return redisPubSub.publish(channel, serverPacket)
                          .then(redisPubSub.publish(Channels.PROXIES, packet))
                          .doFirst(() -> logger.info("Announcing new server {}", serverInfo.id()));
    }

    private Mono<ServerInfo> getServerInfo(String id) {
        return redisRepository.get(RedisKeys.serversKey(), id, ServerInfo.class);
    }

    private ServerInfo createInfo(ServerRegisterRequestPacket packet) {
        ServerInfo info = packet.getServerInfo();
        return new ServerInfo(info.id(), info.serverType(), info.host(), info.port(), info.playersCount(), info.tps(), info.lastHeartbeat());
    }
}
