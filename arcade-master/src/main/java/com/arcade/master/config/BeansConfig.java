package com.arcade.master.config;

import com.arcade.redis.*;
import com.arcade.redis.converter.Converter;
import com.arcade.redis.converter.ConverterImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class BeansConfig {

    @Bean
    public Converter converter() {
        return new ConverterImpl(true);
    }

    @Bean
    public RedisConfig redisConfig(@Value("${redis.config.nodeURI}") String nodeURI,
                                   @Value("${redis.config.token}") String password,
                                   @Value("${redis.config.timeout}") Duration timeout) {
        return new RedisConfig(nodeURI, password, timeout);
    }

    @Bean
    public RedisConnectionFactory redisConnectionFactory(RedisConfig redisConfig) {
        return new RedisConnectionFactory(redisConfig);
    }

    @Bean
    public RedisRepository redisRepository(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        return new RedisRepositoryImpl(redisConnectionFactory, converter);
    }

    @Bean
    public RedisPubSub redisPubSub(RedisConnectionFactory redisConnectionFactory, Converter converter) {
        return new RedisPubSubImpl(redisConnectionFactory, converter);
    }
}
