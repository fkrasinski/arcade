package com.arcade.master.scheduler;

import com.arcade.model.Channels;
import com.arcade.model.ServerInfo;
import com.arcade.model.packet.Packet;
import com.arcade.model.packet.ServerUnregisterPacket;
import com.arcade.redis.RedisKeys;
import com.arcade.redis.RedisPubSub;
import com.arcade.redis.RedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;

@Component
public class HeartbeatMonitor {
    private static final Logger logger = LoggerFactory.getLogger(HeartbeatMonitor.class);
    private static final long MAX_DELAY = 10_000;

    private final RedisRepository redisRepository;
    private final RedisPubSub redisPubSub;

    public HeartbeatMonitor(RedisRepository redisRepository,
                            RedisPubSub redisPubSub) {
        this.redisRepository = redisRepository;
        this.redisPubSub = redisPubSub;
    }

//    @Scheduled(fixedRate = 5_000)
    public void monitorHeartbeat() {
        redisRepository.getAllByKey(RedisKeys.serversKey(), ServerInfo.class)
                       .flatMapIterable(Map::entrySet)
                       .filter(entry -> !isAlive(entry.getValue()))
                       .map(Map.Entry::getKey)
                       .flatMap(this::announceDead)
                       .blockLast();
    }

    private Mono<Void> announceDead(String id) {
        return redisRepository.delete(RedisKeys.serversKey(), id)
                              .then(redisPubSub.publish(Channels.PROXIES, mapToPacket(id)))
                              .doFirst(() -> logger.warn("Announcing server {} as dead", id));
    }

    private Packet mapToPacket(String id) {
        return new ServerUnregisterPacket(id);
    }

    private boolean isAlive(ServerInfo info) {
        return System.currentTimeMillis() < info.lastHeartbeat() + MAX_DELAY;
    }
}
